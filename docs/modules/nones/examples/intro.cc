#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void intro()
{
  // tag::nones_tag0[]
  constexpr double dbl_none = prim_traits<double>::none;
  const column<double> xs( { 10.0, 20.0, dbl_none, 30.0 } );
  cout << xs << "\n";

  constexpr unsigned int uint_none = prim_traits<unsigned int>::none;
  const column<unsigned int> ys( { 10, 20, uint_none, 40 });
  cout << ys << "\n";
  // end::nones_tag0[]

  // tag::nones_is_none[]
  const bool is_none = prim_traits<double>::is_none(xs[2]);
  cout << is_none << "\n";
  // end::nones_is_none[]
  //
  // tag::nones_zxs[]
  const column<double> zxs = xs.zfill();
  cout << zxs << "\n";

  const column<unsigned int> zys = ys.zfill();
  cout << zys << "\n";
  // end::nones_zxs[]
}
