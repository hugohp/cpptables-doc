#include <cpptables/table.hh>
#include <cpptables/where.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct col0  { constexpr static char const * const name = "col0"; };
struct col1  { constexpr static char const * const name = "col1"; };
struct col2  { constexpr static char const * const name = "col2"; };
struct col3  { constexpr static char const * const name = "col3"; };

int main()
{
  using table_t = table<
    pair<col0,std::string_view>,
    pair<col1,unsigned>,
    pair<col2,double>
  >;

  const table_t t(
    {"A", "B", "C", "A", "B", "C", "A", "B", "C"},
    {1,10,100,2,20,200,3,30,300},
    {100.0,200.0,300.0,400.0,500.0,600.0,700.0,800.0,900.0}
  );

  // Filter rows
  {
    // Select rows whose col0 is A
    const table_t t0 = t.at( where( t.col<col0>() == std::string_view("A"))); 
    cout << "select where col0 == A:\n" << t0 << "\n";

    // Same query, different syntax
    const table_t t1 = t.at( t.col<col0>().eq(std::string_view("A")).where() );

    // Select rows whose col0 > A
    const table_t t2 = t.at( t.col<col0>().gt(std::string_view("A")).where() );
    cout << "select where col0 > A:\n" << t2 << "\n";
  }

  // ================ Grouping with group_by ================

  using gtable_t = table_t::group_by_t<std::tuple<col0>>;
  const gtable_t gt = t.group_by<col0>();

  // Sum col1,col2 grouped by col0
  {
    const table_t t_sum = t.group_by<col0>().sum();
    cout << "sum col1,col2 by col0:\n" << t_sum << "\n";
  }

  // ================ apply aggregators or transformation to groups: fby  ================

  // Sum col1,avg col2 by col0
  {
    const auto fsum = [](const column<auto>& xs) { return xs.sum(); };
    const auto favg = [](const column<auto>& xs) { return xs.avg(); };
    const table_t t1 = t.fby<col0>().agg<col1,col2>(fsum,favg);
    cout << "sum col1,avg col2 by col0:\n" << t1 << "\n";
  }

  // Reverse col1 by col0
  {
    const auto freverse = [](const column<auto>& xs) { return xs.reverse(); };
    const table_t t1 = t.fby<col0>().fupdate<col1>(freverse);
    cout << "reverse col1 by col0:\n" << t1 << "\n";
  }

  // ================ Joins ================

  using table1_t = table<
    pair<col0,std::string_view>,
    std::pair<col3,std::string_view>
  >;

  const table1_t t1(
   {"A", "B"},
   {"alfa","beta"}
  );

  // Left join t with t1 on col0
  {
    const auto t_lj = t.lj( t1.key_by<col0>() );
    cout << "left join t with t1:\n" << t_lj << "\n";
  }
 
  // Inner join t with t1 on col0
  {
    const auto t_ij = t.ij( t1.key_by<col0>() );
    cout << "inner join t with t1:\n" << t_ij << "\n";
  }

  return 0;
}
