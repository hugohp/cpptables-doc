// tag::fupdate_by_intro[]
#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,char>,
  pair<c2,unsigned>
>;
// end::fupdate_by_intro[]

void fupdate_by()
{
  // tag::fupdate_by_t0[]
  constexpr char none_char = prim_traits<char>::none;
  const table_t t0(
      {"abc", "def", "ghi", "abc", "def", "ghi","abc", "def", "ghi", "abc", "def", "ghi"},
      {'a','b','c','a', none_char, none_char, none_char, none_char, none_char,'x','y','z'},
      {2,20,200,1,10,100,3,30,30,5,50,500}
    );
  cout << t0 << "\n";
  // end::fupdate_by_t0[]

  // tag::fupdate_by_t1[]
  const auto ffills = [](const column<auto>& c) { return c.fills(); };
  const table_t t1 = t0.fupdate_by<c0>().f<c1>(ffills);
  cout << t1 << "\n";
  // end::fupdate_by_t1[]
}
