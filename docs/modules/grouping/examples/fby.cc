// tag::fby_intro[]
#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct col0  { constexpr static char const * const name = "col0"; };
struct col1  { constexpr static char const * const name = "col1"; };
struct col2  { constexpr static char const * const name = "col2"; };

using table_t = table<
  pair<col0,string_view>,
  pair<col1,unsigned>,
  pair<col2,double>
>;
// end::fby_intro[]

void fby()
{
  // tag::fby_t[]
  const table_t t(
    {"A", "B", "C", "A", "B", "C", "A", "B", "C"},
    {1,10,100,2,20,200,3,30,300},
    {100.0,200.0,300.0,400.0,500.0,600.0,700.0,800.0,900.0}
  );
  cout << t << "\n";
  // end::fby_t[]

  // tag::fby_t1[]
  const auto fsum = [](const column<auto>& xs) { return xs.sum(); };
  const auto favg = [](const column<auto>& xs) { return xs.avg(); };
  const table_t t1 = t.fby<col0>().agg<col1,col2>(fsum,favg);
  cout << "sum col1,avg col2 by col0:\n" << t1 << "\n";
  // end::fby_t1[]

  // tag::fby_t2[]
  const auto freverse = [](const column<auto>& xs) { return xs.reverse(); };
  const table_t t2 = t.fby<col0>().fupdate<col1>(freverse);
  cout << "reverse col1 by col0:\n" << t2 << "\n";
  // end::fby_t2[]
}

