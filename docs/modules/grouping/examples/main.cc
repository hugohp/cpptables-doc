#include <iostream>

extern void group_by();
extern void fby();

using namespace std;

int main()
{
  cout << "group_by:\n";
  group_by();

  cout << "fby:\n";
  fby();

  return 0;
}

