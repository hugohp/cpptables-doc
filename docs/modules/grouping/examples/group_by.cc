// tag::group_by_intro[]
#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>,
  pair<c2,unsigned>
>;
// end::group_by_intro[]

void group_by()
{
    // tag::group_by_t0[]
  const table_t t0(
    {"abc", "def", "ghi", "abc", "def", "ghi","abc", "def", "ghi", "abc", "def", "ghi"},
    {1,2,3,4,5,6,7,8,9,10,11,12},
    {2,20,200,1,10,100,5,50,500,3,30,300}
  );
  cout << t0 << "\n";
  // end::group_by_t0[]

  // tag::group_by_g0[]
  using gtable_t = table_t::group_by_t<tuple<c0>>;

  const gtable_t g0 = t0.group_by<c0>();
  cout << g0 << "\n";
  // end::group_by_g0[]

  // tag::group_by_g1[]
  const gtable_t g1 = g0.sort_by<c2>();
  cout << g1 << "\n";
  // end::group_by_g1[]
 
  // tag::group_by_t1[]
  const table_t t1 = g1.ungroup();
  cout << t1 << "\n";
  // end::group_by_t1[]

  // tag::group_by_t2[]
  const table_t t2 = g0.max();
  cout << t2 << "\n";
  // end::group_by_t2[]
}
