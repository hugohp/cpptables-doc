:source-highlighter: highlight.js
= tables::table::add

1)
[source,cpp]
----
template <typename... Ts,is_primitive_type... Ps>
template<typename... TAs,is_primitive_type... PAs>
requires (
  (!in<TAs,std::tuple<Ts...>> && ... ) &&
  is_tuple_unique<std::tuple<TAs...>> &&
  ( sizeof...(TAs) == sizeof...(PAs) )
)
inline
constexpr auto
table<std::pair<Ts,Ps>...>::add(const column<PAs>&... cs) const
----
''''
2)
[source,cpp]
----
template <typename... Ts,is_primitive_type... Ps>
template<typename... TAs,is_primitive_type... PAs>
requires (
  (!in<TAs,std::tuple<Ts...>> && ... ) &&
  is_tuple_unique<std::tuple<TAs...>> &&
  ( sizeof...(TAs) == sizeof...(PAs) )
)
inline
constexpr auto
table<std::pair<Ts,Ps>...>::add(
  const table<std::pair<TAs,PAs>...>& t) const
----
''''

1. Returns new table obtained by adding the xref:../column.adoc[column](s) to the right of existing table.
2. Returns new table obtained by adding xref:../column.adoc[columns] of _t_ to the right of existing table.

The lookup-types used must not already be present in table. To add-or-update, use xref:table_addu.adoc[addu], to update an existing column use xref:table_update.adoc[update], or xref:table_update_at.adoc[update_at].

== Example
=== Code
[source,cpp]
----
include::example$table_add.cc[]
----
=== Output

----
Col0 Col1 Col2
--------------
 abc   10    1
 def   20    2
 ghi   30    3
----
