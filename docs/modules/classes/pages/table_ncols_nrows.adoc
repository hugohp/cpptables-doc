:source-highlighter: highlight.js
= tables::table::ncols,nrows

1)
[source,cpp]
----
template <typename... Ts,is_primitive_type... Ps>
inline static constexpr std::size_t 
table<std::pair<Ts,Ps>...>::ncols()
----
''''

2)
[source,cpp]
----
template <typename... Ts,is_primitive_type... Ps>
std::size_t
table<std::pair<Ts,Ps>...>::nrows() const
----
''''

1. Returns number of columns.
2. Returns number of rows.

== Example
=== Code
[source,cpp]
----
include::example$table_ncols_nrows.cc[]
----
=== Output

----
4
----
