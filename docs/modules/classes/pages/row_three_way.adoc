:source-highlighter: highlight.js
= tables::row::operator \<\=>

[source,cpp]
----
template <typename... Ks, typename... Vs>
auto operator<=>(
  const row<std::pair<Ks,Vs>...>& lhs,
  const row<std::pair<Ks,Vs>...>& rhs)
)
----

Invokes three-way comparison of underlying xref:row.adoc#mtuple_t[mtuple_t].

== Example
=== Code
[source,cpp]
----
include::example$row_three_way.cc[]
----
=== Output
----
1
----
