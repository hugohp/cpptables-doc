= tables::column::norm

[source,cpp]
----
template <is_primitive_type P>
double column<P>::norm() const
----
'''

Returns the Euclidean norm of the elements in the column.

== Complexity

Linear

== Example
=== Code
[source,cpp]
----
include::example$column_norm.cc[]
----
=== Output

----
2.82843
----
