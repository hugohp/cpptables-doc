= tables::row::any

[source,cpp]
----
template <typename... Ts, is_primitive_type... Ps>
inline
bool
row<std::pair<Ts,Ps>...>::any() const
----
'''

Returns true if at least one element is true (boolean-types), empty-string (string_view), or non-zero (arithmetic types).

== Complexity
Linear

== Example
=== Code
[source,cpp]
----
include::example$row_any.cc[]
----
=== Output
----
1
1
1
----
