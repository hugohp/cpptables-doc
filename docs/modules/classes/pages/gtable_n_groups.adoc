:source-highlighter: highlight.js
= tables::gtable::n_groups

[source,cpp]
----
template <typename... Ts,is_primitive_type... Ps,typename... Gs>
std::size_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::n_groups() const
----

''''

Returns the number of groups.

== Example
=== Code
[source,cpp]
----
include::example$gtable_n_groups.cc[]
----
=== Output

----
3
----
