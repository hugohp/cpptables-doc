= tables::column::sums

1)
[source,cpp]
----
template <is_primitive_type P>
column<P> column<P>::sums()
----
'''
2)
[source,cpp]
----
template <is_primitive_type P>
column<P> column<P>::msum(const std::size_t m) const
----
'''

1. Returns the cumulative sum of items.
2. Returns moving sum of the last _m_ items.

== Complexity
Linear

== Example
=== Code
[source,cpp]
----
include::example$column_sums.cc[]
----
=== Output

----
1 2 6 10 15

1 3 5 7 9
----
