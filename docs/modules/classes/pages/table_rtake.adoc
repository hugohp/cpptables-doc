:source-highlighter: highlight.js
= tables::table::rtake

[source,cpp]
----
template <typename... Ts,is_primitive_type... Ps>
inline
constexpr table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::rtake(const std::size_t n) const
----
''''

Returns table with last _n_ rows. Same as reverse().take(n).reverse().

== Example
=== Code
[source,cpp]
----
include::example$table_rtake.cc[]
----
=== Output

----
Col0 Col1
---------
 def   50
 ghi   60
----
