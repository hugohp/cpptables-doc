:source-highlighter: highlight.js
= tables::table::append,_append

[source,cpp]
----
template <typename... Ts,is_primitive_type... Ps>
inline
void
table<std::pair<Ts,Ps>...>::_append(const row_t& row)

----
''''

Appends xref:../table/row.adoc[row] to existing table.
