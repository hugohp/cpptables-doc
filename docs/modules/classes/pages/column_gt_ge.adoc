= tables::column::gt,ge

1)
[source,cpp]
----
template<is_primitive_type P>
inline
column<bool_t>
column<P>::gt(const column<P>& rhs) const
----
'''

2)
[source,cpp]
----
template<is_primitive_type P>
inline
column<bool_t>
column<P>::gt(const P cte) const
----
'''


3)
[source,cpp]
----
template<is_primitive_type P>
inline
column<bool_t>
column<P>::ge(const column<P>& rhs) const
----
'''

4)
[source,cpp]
----
template<is_primitive_type P>
inline
column<bool_t>
column<P>::ge(const P cte) const
----
'''

1 and 2) Returns column-of-bools containing column-wise greater-than comparison between this and `rhs` columns or between this and `cte` value.


3 and 4) Returns column-of-bools containing column-wise greater-or-equal comparison between this and `rhs` columns or between this and `cte` value.

== Complexity
O(n)

== Example
=== Code
[source,cpp]
----
include::example$column_gt_ge.cc[]
----
=== Output

----
False False False False False
True True False False False
False False False True True
False False True True True
----
