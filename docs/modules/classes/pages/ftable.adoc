:source-highlighter: highlight.js
= tables::ftable::fupdate

[source,cpp]
----
template <is_table,tuple_ext::is_tuple Gs>
requires ( std::tuple_size_v<Gs> > 0 )
struct ftable
----

Used to apply an aggregate or transformation to a group.

It is not recommended to construct an ftable directly, but to use xref:table_fby.adoc[table::fby]. See xref:grouping:fby.adoc[Grouping] for examples.

The implementation stores a map of _row<pair<Gs,PGs>...>_ to a vector-of-indices. The vector-of-indices are the indices for the rows that contain same values at column Gs.

== Member types.

[cols="1,1"]
|===
|Member type|Definition

|ts_t
|Tuple of lookup-types of original table.

|[[table_t]]table_t
|Original table type.

|gs_t
|Tuple of group lookup-types.

|pgs_t
|Tuple of primitive-types matching _gs_t_.

|ftable_t
|ftable type.

|g_row_t
|xref:row.adoc[row] type for pair gs_t,pgs_t.

|map_t
|https://en.cppreference.com/w/cpp/container/map[map] of _g_row_t_ to vector-of-indices.

|===

== Member functions
=== Constructors / Assignment

[cols="1,1"]
|===
|Member type|Definition

|xref:ftable_ctrs.adoc[Constructors]
|Constructos the ftable.

|xref:ftable_assign.adoc[Assignment operator]
|Assigns values to a ftable.

|===

=== Capacity
 
[cols="1,1"]
|===
|Member type|Definition

|xref:ftable_n_groups.adoc[n_groups]
|Returns number of groups.

|===

=== Accessors
 
[cols="1,1"]
|===
|Member type|Definition

|xref:ftable_m.adoc[m]
|Returns underlying map.

|xref:ftable_t.adoc[t]
|Returns original table.

|===


=== Aggregate
 
[cols="1,1"]
|===
|Member type|Definition

|xref:ftable_agg.adoc[agg]
|Aggregates selected columns using selected aggregate function.

|===

=== Transform
 
[cols="1,1"]
|===
|Member type|Definition

|xref:ftable_fupdate.adoc[fupdate]
|Apply transformation on selected columns using selected transformation function.

|===
