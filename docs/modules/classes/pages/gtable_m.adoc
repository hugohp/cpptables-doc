:source-highlighter: highlight.js
= tables::gtable::n_groups

[source,cpp]
----
template <typename... Ts,is_primitive_type... Ps,typename... Gs>
typename gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::map_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::m() const
----

''''

Returns underlyung map.

== Example
=== Code
[source,cpp]
----
include::example$gtable_m.cc[]
----

=== Output

----
row:
Col0
-----
  abc

table:
Col1 Col2
---------
 100    1
 400    4


row:
Col0
-----
  def

table:
Col1 Col2
---------
 200    2
 500    5

row:
Col0
-----
 ghi

table:
Col1 Col2
---------
 300    3
 600    6
----
