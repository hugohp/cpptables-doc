:source-highlighter: highlight.js
= tables::table::avg

[source,cpp]
----
template <typename... Ts,is_primitive_type... Ps>
inline
table<std::pair<Ts,Ps>...>::row_t
table<std::pair<Ts,Ps>...>::avg() const
{
----
''''

Returns row containing arithmetic mean of elements of each column.

== Example
=== Code
[source,cpp]
----
include::example$table_sum.cc[]
----
=== Output

----
Col0 Col1
---------
   2   20
----
