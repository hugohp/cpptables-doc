= tables::table::gt,ge

1)
[source,cpp]
----
template <typename... Ts,is_primitive_type... Ps>
inline
auto
table<std::pair<Ts,Ps>...>::gt(const table<std::pair<Ts,Ps>...>& rhs) const
----
'''

2)
[source,cpp]
----
template <typename... Ts,is_primitive_type... Ps>
inline
auto
table<std::pair<Ts,Ps>...>::ge(const table<std::pair<Ts,Ps>...>& rhs) const
----
''''

Returns table-of-bools containing element-wise greater-than/greater-or-equal comparison between this and `rhs` columns.

== Complexity
O(n)

== Example
=== Code
[source,cpp]
----
include::example$table_gt_ge.cc[]
----

=== Output
----
Col0  Col1
-----------
False False
False False
 True  True

Col0 Col1
---------
True True
True True
True True
----
