:source-highlighter: highlight.js
= tables::table::uj

[source,cpp]
----
template <typename... Ts,is_primitive_type... Ps>
template<is_table T>
requires ( is_joinable<table<std::pair<Ts,Ps>...>,T> )
inline
auto
table<std::pair<Ts,Ps>...>::uj(const T& t) const
----
''''

Returns a table with concatenated rows of left and right table. Columns are filled with none if needed.

== Example
=== Code
[source,cpp]
----
include::example$table_uj.cc[]
----
=== Output

----
t:
Col0 Col1 Col2
--------------
 abc  100 none
 def  200 none
 ghi  300 none
 abc  400 none
 def  500 none
 ghi  600 none
 abc none    1
 def none    2
 foo none    3
----
