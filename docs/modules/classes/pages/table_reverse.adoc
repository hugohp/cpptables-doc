:source-highlighter: highlight.js
= tables::table::reverse

[source,cpp]
----
template <typename... Ts,is_primitive_type... Ps>
inline
constexpr table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::reverse() const
----
''''

Returns table with rows in reverse order.

== Example
=== Code
[source,cpp]
----
include::example$table_reverse.cc[]
----
=== Output

----
Col0 Col1
---------
 ghi   60
 def   50
 abc   40
 ghi   30
 def   20
 abc   10

----
