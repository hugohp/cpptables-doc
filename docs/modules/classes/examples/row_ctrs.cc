#include <cpptables/table.hh>

using namespace tables;
using namespace std;

struct name_c { constexpr static string_view name = "Name"; };
struct mass_c { constexpr static string_view name = "Mass"; };
struct radius_c { constexpr static string_view name = "Radius"; };
  
// The table type
using row_t = row<
  pair<name_c,string_view>,
  pair<mass_c,double>,
  pair<radius_c,double>
>;

void row_ctrs()
{
  // Default constructor.
  row_t empty;

  // Construct with values
  const row_t mercury("Mercury", 0.055, 0.3829);

  using ps_t = typename row_t::ps_t;

  // Construct with tuple of primitive-types
  ps_t ps = make_tuple<string_view,double,double>("Venus",0.815,0.9499);
  const row_t venus(ps);

  // Construct with tuple of primitive-types (by rvalue)
  row_t venus2(move(ps));

  // Copy constructor
  row_t mercury2(mercury);

  // Move constructor
  row_t mercury3(move(mercury2));
}
