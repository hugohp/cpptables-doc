#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>
>;

void table_distinct()
{
  const table_t t(
      {"abc","def","abc","ghi","ghi"},
      {10,20,10,30,40}
    );
  
  cout << t.distinct() << "\n";
}
