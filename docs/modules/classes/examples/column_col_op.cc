#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_col_op()
{
  const column<unsigned> xs ({ 1, 2, 3, 4, 5});
  const column<unsigned> ys ({ 10, 20, 30, 40, 50});

  const column<unsigned> res0 = xs + ys;
  cout << res0 << "\n";

  const column<unsigned> res1 = xs + 100U;
  cout << res1 << "\n";
}
