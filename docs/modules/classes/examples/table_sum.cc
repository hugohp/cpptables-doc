#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };

using table_t = table<
  pair<c0,unsigned>,
  pair<c1,double>
>;

void table_sum()
{
  const table_t t(
      {1,2,3},
      {10.0,20.0,30.0}
    );
  
  const table_t::row_t s = t.sum();
  cout << s << "\n";
}
