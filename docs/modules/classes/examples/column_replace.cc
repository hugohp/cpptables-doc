#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_replace()
{
  constexpr unsigned none = prim_traits<unsigned>::none;

  const column<unsigned> c0 ({ 1, none, 3, 4 ,5 });
  const column<unsigned> c1 ({ 10, 20, none, 40, 50});

  cout << c0.replace(c1) << "\n";
}
