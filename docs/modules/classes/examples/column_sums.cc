#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_sums()
{
  vector<unsigned> v(5);
  iota(v.begin(), v.end(), 1);
  const column<unsigned> xs(v);

  cout << xs.sums() << "\n";
  cout << xs.msum(2) << "\n";
}
