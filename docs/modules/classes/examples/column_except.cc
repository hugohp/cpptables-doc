#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_except()
{
  const column<unsigned> xs ({ 1, 2, 3, 4, 5});

  const column<unsigned> ys ({ 2, 3});
  const column<unsigned> zs = xs.except(ys);
  cout << zs << "\n";
}
