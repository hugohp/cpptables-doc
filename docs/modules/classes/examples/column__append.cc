#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column__append()
{
  column<unsigned> xs ({ 1, 2, 3, 4, 5});

  xs._append(6);
  cout << xs << "\n";
}
