#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_rdrop()
{
  constexpr unsigned none = prim_traits<unsigned>::none;

  const column<unsigned> xs ({ 1, 2, 3, 4, 5, none});

  cout << xs.rdrop(2) << "\n";
}
