#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_reverse()
{
  const column<unsigned> xs ({ 1, 2, 3, 4, 5});
  const column<unsigned> ys = xs.reverse();
  cout << ys << "\n";
}
