#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_op()
{
  column<double> c0({10.0, 20.0, 30.0});
  cout << c0[1] << "\n";
}
