#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_apply()
{
  const column<unsigned> xs ({ 1, 2, 3, 4, 5});

  const auto f = [] (unsigned x) { return 2*x; };
  const column<unsigned> ys = xs.apply(f);
  cout << ys << "\n";
}
