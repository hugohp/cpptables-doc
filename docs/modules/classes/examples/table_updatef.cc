#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>,
  pair<c2,unsigned>
>;


void table_updatef()
{
  constexpr unsigned none_uint = prim_traits<unsigned>::none;
  const table_t t(
      {"abc", "def", "ghi"},
      {1,2,3},
      {10,20,30}
    );

  const column<unsigned> col({100,none_uint,300});
  const table_t t1 = t.updatef<c1,c2>(col,col);
  cout << t1 << "\n";
}
