#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_to_string()
{
  using sv_column = column<std::string_view>;

  const sv_column xs({ 
    "abc",
    "def",
    "ghi"
  });


  cout << xs.to_string("_") << "\n";
}
