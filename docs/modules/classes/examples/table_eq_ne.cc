#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,double>
>;

void table_eq_ne()
{
  const table_t t0(
    {"abc","def","ghi"},
    {10.0, 20.0, 30.0}
  );
  
  const table_t t1(
    {"abc","def","ghi"},
    {10.0, 20.0, 0.0}
  );
 
  const auto t_eq = t0.eq(t1); // same as calling t0 == t1
  cout << t_eq << "\n";

  const auto t_ne = t0.ne(t1); // same as calling t0 != t1
  cout << t_ne << "\n";
}
