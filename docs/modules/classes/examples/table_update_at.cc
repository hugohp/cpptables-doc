#include <cpptables/table.hh>
#include <cpptables/where.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>
>;


void table_update_at()
{
  const table_t t(
      {"abc", "def", "ghi", "abc", "def", "ghi"},
      {100,200,300,400,500,600}
    );

  const column<unsigned> vals({1,2,3,4,5,6});
  const auto t1 = t.update_at<c1>( 
    where( t.col<c0>() == string_view("abc") ), 
    vals
  );
  cout << t1 << "\n";
}
