#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>,
  pair<c2,double>
>;


void table_addu()
{
  const table_t t(
      {"abc", "def", "ghi"},
      {100,200,300},
      {1.0, 2.0, 3.0} 
    );

  const auto t1 = t.addu<c2>( column<string_view>({"foo","bar","xyz"}) );
  cout << t1 << "\n";
}
