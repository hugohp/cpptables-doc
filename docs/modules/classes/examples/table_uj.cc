#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };

using table0_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>
>;

using table1_t = table<
  pair<c0,string_view>,
  pair<c2,double>
>;

void table_uj()
{
  const table0_t t0(
      {"abc", "def", "ghi", "abc", "def", "ghi"},
      {100,200,300,400,500,600}
  );

  const table1_t t1(
      {"abc", "def", "foo"},
      {1,2,3}
  );

  const auto t = t0.uj( t1 );
  cout << "t:\n" << t << "\n";
}
