#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_differ()
{
  const column<unsigned> xs ({ 10, 10, 20, 20, 21, 21});
  const column<bool> ds = xs.differ();
  cout << ds << "\n";
}
