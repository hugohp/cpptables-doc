#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_distinct()
{
  const column<string_view> xs ({ "foo", "bar", "bar" });
  const auto ys = xs.distinct();
  
  cout << ys << "\n";
}
