#include <cpptables/table.hh>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>
>;

void gtable_deltas()
{
  const table_t t(
      {"abc", "def", "ghi", "abc","def","ghi","abc","def","ghi"},
      {1,10,100,2,20,200,5,50,500}
    );

  using gtable_t = table_t::group_by_t<tuple<c0>>;

  const gtable_t gt = t.group_by<c0>().deltas();
  cout << gt << "\n";
}
