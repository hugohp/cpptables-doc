#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_sort()
{
  const column<unsigned> xs ({ 2,3,1,5,4 });

  cout << xs.sort() << "\n";
  cout << xs.rsort() << "\n";
}
