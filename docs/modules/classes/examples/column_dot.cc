#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_dot()
{
  const column<unsigned> xs({ 10, 5, 3});
  const column<unsigned> ys({ 0, 1, 2});

  cout << xs.dot(ys) << "\n";
}
