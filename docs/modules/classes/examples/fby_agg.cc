#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct col0  { constexpr static char const * const name = "col0"; };
struct col1  { constexpr static char const * const name = "col1"; };
struct col2  { constexpr static char const * const name = "col2"; };

using table_t = table<
  pair<col0,string_view>,
  pair<col1,unsigned>,
  pair<col2,double>
>;

void fby_agg()
{
  const table_t t(
    {"A", "B", "C", "A", "B", "C", "A", "B", "C"},
    {1,10,100,2,20,200,3,30,300},
    {100.0,200.0,300.0,400.0,500.0,600.0,700.0,800.0,900.0}
  );

  const auto fsum = [](const column<auto>& xs) { return xs.sum(); };
  const auto t1 = t.fby<col0>().agg<col1>(fsum);
  cout << t1 << "\n";

  const auto favg = [](const column<auto>& xs) { return xs.avg(); };
  const auto t2 = t.fby<col0>().agg<col1,col2>(fsum,favg);
  cout << t2 << "\n";
}
