#include <iostream>
#include <cpptables/table.hh>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,double>
>;

void table_append()
{
  const table_t t(
      {"abc","def","ghi"},
      {10.0, 20.0, 30.0}
    );

  // Append row
  const table_t::row_t row("xyz", 123.0);

  const table_t t0 = t.append(row);
  cout << t0 << "\n";

  // Append table
  const table_t t1(
      {"foo", "bar"},
      {-100.0, 100.0}
    );

  const table_t t2 = t.append(t1);
  cout << t2 << "\n";
}
