#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_at()
{
  column<double> c0({10.0, 20.0, 30.0});
  cout << c0.at( vixs_t({0,2}) ) << "\n";
}
