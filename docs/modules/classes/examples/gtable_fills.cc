#include <cpptables/table.hh>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>,
  pair<c2,double>
>;



void gtable_fills()
{
  constexpr unsigned uint_none = prim_traits<unsigned>::none;
  constexpr double dbl_none = prim_traits<double>::none;

  const table_t t(
      {"abc", "def", "ghi", "abc","def","ghi","abc","def","ghi"},
      {100,200,300,uint_none, uint_none, uint_none, 700,800,900},
      {1.0, 2.0, 3.0, 4.0,5.0,6.0,dbl_none, dbl_none, dbl_none}
    );

  using gtable_t = table_t::group_by_t<tuple<c0>>;
  using ngrow_t = gtable_t::ngrow_t;

  const gtable_t gt = t.group_by<c0>();

  cout << gt.fills() << "\n";
  cout << gt.fill_with( ngrow_t( 999, 9.9) ) << "\n";
  cout << gt.zfill() << "\n";
}
