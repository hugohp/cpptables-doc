#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_next_prev()
{
  const column<unsigned> xs({ 1, 2, 3, 4, 5, 6 });

  cout << xs.next() << "\n";
  cout << xs.prev() << "\n";
}
