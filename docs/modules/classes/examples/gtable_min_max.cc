#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>,
  pair<c2,double>
>;

void gtable_min_max()
{
  const table_t t(
      {"abc","def","ghi","abc","def","ghi","abc","def","ghi"},
      {10,100,1000,10,100,1000,10,100,1000},
      {2.0,2.0,2.0,3.0,3.0,3.0,1.0,1.0,1.0}
    );
  
  const table_t gtmin = t.group_by<c0>().min();
  cout << gtmin << "\n";

  cout << "\n";

  const table_t gtmax = t.group_by<c0>().max();
  cout << gtmax << "\n";
}
