#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_fills()
{
  constexpr unsigned none = prim_traits<unsigned>::none;

  const column<unsigned> xs ({ 1, none, 3, 4 ,5, none });

  cout << xs.fills() << "\n";
  cout << xs.fill_with(99) << "\n";
  cout << xs.zfill() << "\n";

  const column<unsigned> ys ({ 10,20,30,40,50, 60});
  cout << xs.fill_with(ys) << "\n";
}
