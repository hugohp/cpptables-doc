#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_mmin_mmax()
{
  const column<unsigned> xs({ 10, 2, 1, 8, 20, 7, 15, 0});

  cout << xs.mmin(2) << "\n";
  cout << xs.mmax(2) << "\n";
}
