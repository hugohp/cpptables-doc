#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_ctrs()
{
  // Constructor with initializer_list
  column<double> c0({10.0, 20.0, 30.0});
  cout << c0 << "\n";

  // Constructor with vector
  vector<unsigned> v(5);
  iota(v.begin(), v.end(), 1);
  const column<unsigned> c1(v);

  // Constructor with vector by rvalue
  vector<unsigned> prims({3,5,7,11});
  const column<unsigned> c2( move(prims) );

  cout << c2 << "\n";
}
