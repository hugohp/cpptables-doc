#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>,
  pair<c2,double>
>;


void gtable_contains()
{
  const table_t t(
      {"abc", "def", "ghi", "abc", "def", "ghi"},
      {100,200,300,400,500,600},
      {1.0,2.0,3.0,4.0,5.0,6.0}
    );

  using gtable_t = table_t::group_by_t<tuple<c0>>;
  using grow_t = gtable_t::grow_t;

  const gtable_t gt = t.group_by<c0>();

	cout << gt.contains( grow_t( string_view("abc") ) ) << "\n";
	cout << gt.contains( grow_t( string_view("foo") ) ) << "\n";
}
