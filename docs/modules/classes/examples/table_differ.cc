#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Differ"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>
>;

void table_differ()
{
  const table_t t(
      {"abc","abc", "abc", "def", "ghi", "ghi"},
      {10, 10, 20, 30, 40 ,40}
    );
  
  const column<bool> ds = t.differ();
  const auto t1 = t.add<c2>(ds);
  cout << t1 << "\n";
}
