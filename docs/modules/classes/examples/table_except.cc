#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,double>
>;

void table_except()
{
  const table_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 10.0, 20.0, 30.0}
    );
  
  cout << t.except( table_t({"abc"},{10.0} )) << "\n";
}
