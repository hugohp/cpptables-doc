#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_mins_maxs()
{
  const column<unsigned> xs({ 5, 2, 10, 8 });

  cout << xs.mins() << "\n";
  cout << xs.maxs() << "\n";
}
