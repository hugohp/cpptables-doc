#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_xbar()
{
  vector<unsigned> v(20);
  iota(v.begin(), v.end(), 0);

  const column<unsigned> xs(v);
  cout << xs.xbar(3) << "\n";
  cout << xs.cast_to<double>().xbar(1.5) << "\n";
}
