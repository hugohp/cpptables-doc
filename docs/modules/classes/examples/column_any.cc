#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_any()
{
  cout << column<bool>({true,false,true}).any() << "\n";
  cout << column<string_view>({"foo","bar",""}).any() << "\n";
  cout << column<unsigned>({0,0,0}).any() << "\n";
}
