#include <cpptables/table.hh>

using namespace tables;
using namespace std;

// Columns
struct name_c   { constexpr static string_view name =  "Name"; };
struct mass_c   { constexpr static string_view name =  "Mass"; };
struct radius_c { constexpr static string_view name =  "Radius"; };
  
// The table type
using planets_t = table<
  pair<name_c,string_view>,
  pair<mass_c,double>,
  pair<radius_c,double>
>;

void table_ctrs()
{
  // Default constructor.
  planets_t no_planets;

  // Construct with initializer_list
  {
    const planets_t planets(
      {"Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"}, 
      {0.055, 0.815, 1.0, 0.107, 317.8, 95.159, 14.536, 17.147 },
      {0.3829, 0.9499, 1.0, 0.533, 11.209, 9.449, 4.007, 3.883 }
    );
    cout << planets << "\n";
  }

  // Construct with colums
  {
    const column<string_view> names({
      "Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"
    });
  
    const column<double> mass({
      0.055, 0.815, 1.0, 0.107, 317.8, 95.159, 14.536, 17.147
    });
  
    const column<double> radius({
      0.3829, 0.9499, 1.0, 0.533, 11.209, 9.449, 4.007, 3.883
    });

    const planets_t planets1( names, mass, radius );
  }

  // Construct with colums by rvalue
  {
    column<string_view> names({
      "Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"
    });
  
    column<double> mass({
      0.055, 0.815, 1.0, 0.107, 317.8, 95.159, 14.536, 17.147
    });
  
    column<double> radius({
      0.3829, 0.9499, 1.0, 0.533, 11.209, 9.449, 4.007, 3.883
    });

    const planets_t planets1(  
      move(names), move(mass), move(radius)
    );
  }

  // Copy constructor
  {
    const planets_t planets(
      {"Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"}, 
      {0.055, 0.815, 1.0, 0.107, 317.8, 95.159, 14.536, 17.147 },
      {0.3829, 0.9499, 1.0, 0.533, 11.209, 9.449, 4.007, 3.883 }
    );
    const planets_t copy(planets);
  }

  // Move constructor
  {
    planets_t planets(
      {"Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"}, 
      {0.055, 0.815, 1.0, 0.107, 317.8, 95.159, 14.536, 17.147 },
      {0.3829, 0.9499, 1.0, 0.533, 11.209, 9.449, 4.007, 3.883 }
    );
    const planets_t moved_planets(move(planets));
  }
}
