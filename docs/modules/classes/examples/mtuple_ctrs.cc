#include <cpptables/table.hh>

using namespace tables;
using namespace std;

struct k0 {};
struct k1 {};
struct k2 {};
  
// The table type
using mtuple_t = row<
  pair<k0,string_view>,
  pair<k1,double>,
  pair<k2,double>
>;

void mtuple_ctrs()
{
  // Default constructor.
  mtuple_t empty;

  // Construct with values
  const mtuple_t mt0("Foo", 0.1, 0.2);

  using ps_t = typename mtuple_t::ps_t;

  // Construct with tuple of primitive-types
  ps_t ps = make_tuple<string_view,double,double>("Bar",0.3,0.4);
  const mtuple_t mt1(ps);

  // Construct with tuple of primitive-types (by rvalue)
  mtuple_t mt2(move(ps));

  // Copy constructor
  mtuple_t mt3(mt2);

  // Move constructor
  mtuple_t mt4(move(mt2));
}
