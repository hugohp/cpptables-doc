#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };

using table_t = table<
  pair<c0,double>,
  pair<c1,unsigned>
>;

void table_deltas()
{
  const table_t t(
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {3, 5, 7, 11, 13, 17}
    );
  
  cout << t.deltas() << "\n";
}
