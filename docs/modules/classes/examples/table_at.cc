#include <cpptables/table.hh>
#include <cpptables/where.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,double>
>;

void table_at()
{
  const table_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0}
    );

  const vixs_t ixs0({0,2});
  const table_t t0 = t.at(ixs0);
  cout << t0 << "\n";

  // where function returns indices where condition is true
  const vixs_t ixs1 = where( t.col<c0>() == string_view("abc") );
  // Use indices obtained from where
  const table_t t1 = t.at(ixs1);
  cout << t1 << "\n";
}
