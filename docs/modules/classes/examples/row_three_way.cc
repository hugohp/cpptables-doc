#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{};
struct c1{};

using row_t = row<
    pair<c0,string_view>,
    pair<c1,double>
>;

void row_three_way()
{
  const row_t row0( "abc", 100 );
  const row_t row1( "abc", 200 );
  cout << (row0 < row1 ) << "\n";
}
