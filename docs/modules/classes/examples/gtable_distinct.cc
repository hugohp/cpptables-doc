#include <cpptables/table.hh>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>,
  pair<c2,double>
>;



void gtable_distinct()
{
  const table_t t(
      {"abc", "def", "ghi", "abc","def","ghi","abc","def","ghi"},
      {10,20,30,10,20,30,100,200,300},
      {1.0,2.0,3.0,1.0,2.0,3.0,10.0,20.0,30.0}
    );

  using gtable_t = table_t::group_by_t<tuple<c0>>;

  const gtable_t gt = t.group_by<c0>().distinct();
  cout << gt << "\n";
}
