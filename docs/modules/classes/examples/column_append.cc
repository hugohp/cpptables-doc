#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_append()
{
  const column<unsigned> xs ({ 1, 2, 3, 4, 5});

  const column<unsigned> ys = xs.append(1);
  cout << ys << "\n";

  const column<unsigned> zs = xs.append(xs);
  cout << zs << "\n";
}
