#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Differ"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>
>;

void table_min_max()
{
  const table_t t(
      {"abc","abc","def","def","ghi","ghi"},
      {6,5,4,3,2,1}
    );
  
  const table_t::row_t rmin = t.min();
  cout << rmin << "\n";

  cout << "\n";

  const table_t::row_t rmax = t.max();
  cout << rmax << "\n";
}
