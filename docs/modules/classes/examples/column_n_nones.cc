#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_n_nones()
{
  constexpr unsigned none = prim_traits<unsigned>::none;
  const column<unsigned> xs ({ 1, none, 2, 3, none });
  cout << xs.n_nones() << "\n";

}
