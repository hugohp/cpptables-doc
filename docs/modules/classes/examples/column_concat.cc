#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_concat()
{
  using sv_column = column<std::string_view>;

  const sv_column xs({ 
    "abc",
    "def",
    "ghi"
  });


  cout << xs.concat("_foo") << "\n";
  cout << xs.concat( 
    sv_column({"_foo", "_bar", "_foo"})
  ) << "\n";
}
