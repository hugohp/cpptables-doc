#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_cross()
{
  const column<double> c0({ 10.0, 20.0});
  const column<char>   c1({'a','b','c'});

  const pair<column<double>,column<char>> res = c0.cross(c1);
  cout << res.first << "\n";
  cout << res.second << "\n";
}
