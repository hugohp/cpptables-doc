#include <iostream>

extern void column_all();
extern void column_any();
extern void column__append();
extern void column_append();
extern void column_apply();
extern void column_at();
extern void column_avg();
extern void column_cast_to();
extern void column_col_op();
extern void column_ctrs();
extern void column_deltas();
extern void column_differ();
extern void column_distinct();
extern void column_dot();
extern void column_except();
extern void column_fills();
extern void column_replace();
extern void column_take();
extern void column_rtake();
extern void column_min_max();
extern void column_mins_maxs();
extern void column_mmin_mmax();
extern void column_next_prev();
extern void column_n_nones();
extern void column_norm();
extern void column_op();
extern void column_reverse();
extern void column_sort();
extern void column_sums();
extern void column_drop();
extern void column_rdrop();
extern void column_xbar();
extern void column_cross();
extern void column_eq_ne();
extern void column_lt_le();
extern void column_gt_ge();
extern void column_in();
extern void column_starts_ends_with();
extern void column_substr();
extern void column_concat();
extern void column_to_string();
extern void mtuple_get();
extern void mtuple_ctrs();
extern void mtuple_size();
extern void mtuple_three_way();
extern void row_col();
extern void row_ctrs();
extern void row_ncols();
extern void row_three_way();
extern void row_all();
extern void row_any();
extern void table_add();
extern void table_addu();
extern void table_append();
extern void table_at();
extern void table_col();
extern void table_ctrs();
extern void table_deltas();
extern void table_differ();
extern void table_except();
extern void table_eq_ne();
extern void table_lt_le();
extern void table_gt_ge();
extern void table_take();
extern void table_rtake();
extern void table_ij();
extern void table_aj();
extern void table_irow();
extern void table_lj();
extern void table_uj();
extern void table_ncols_nrows();
extern void table_remove();
extern void table_reverse();
extern void table_fills();
extern void table_distinct();
extern void table_sort_by();
extern void table_rsort_by();
extern void table_select();
extern void table_drop();
extern void table_rdrop();
extern void table_update_at();
extern void table_update();
extern void table_updatef();
extern void table_min_max();
extern void table_sum();
extern void table_avg();
extern void gtable_n_groups();
extern void gtable_m();
extern void gtable_contains();
extern void gtable_get();
extern void gtable_take_drop();
extern void gtable_rtake_rdrop();
extern void gtable_ungroup();
extern void gtable_reverse();
extern void gtable_fills();
extern void gtable_deltas();
extern void gtable_distinct();
extern void gtable_sort();
extern void gtable_sort_by();
extern void gtable_rsort_by();
extern void gtable_min_max();
extern void gtable_sum();
extern void gtable_avg();
extern void fby_agg();
extern void fby_fupdate();

using namespace std;

int main()
{
  cout << "\ntable_ctrs" << "\n";
  table_ctrs();

  cout << "column_all:\n";
  column_all();

  cout << "column_any:\n";
  column_any();

  cout << "column__append:\n";
  column__append();

  cout << "column_append:\n";
  column_append();

  cout << "column_apply:\n";
  column_apply();

  cout << "column_at:\n";
  column_at();

  cout << "column_avg:\n";
  column_avg();

  cout << "column_cast_to:\n";
  column_cast_to();

  cout << "column_col_op:\n";
  column_col_op();

  cout << "column_ctrs:\n";
  column_ctrs();

  cout << "column_deltas:\n";
  column_deltas();

  cout << "column_differ:\n";
  column_differ();

  cout << "column_distinct:\n";
  column_distinct();

  cout << "column_dot:\n";
  column_dot();

  cout << "column_except:\n";
  column_except();

  cout << "column_fills:\n";
  column_fills();

  cout << "column_replace:\n";
  column_replace();

  cout << "column_take:\n";
  column_take();

  cout << "column_rtake:\n";
  column_rtake();

  cout << "column_min_max:\n";
  column_min_max();

  cout << "column_mins_maxs:\n";
  column_mins_maxs();

  cout << "column_mmin_mmax:\n";
  column_mmin_mmax();

  cout << "column_next_prev:\n";
  column_next_prev();

  cout << "column_n_nones:\n";
  column_n_nones();

  cout << "column_norm:\n";
  column_norm();

  cout << "column_op:\n";
  column_op();

  cout << "column_reverse:\n";
  column_reverse();

  cout << "column_sort:\n";
  column_sort();

  cout << "column_sums:\n";
  column_sums();

  cout << "column_drop:\n";
  column_drop();

  cout << "column_rdrop:\n";
  column_rdrop();

  cout << "column_xbar:\n";
  column_xbar();

  cout << "column_cross:\n";
  column_cross();

  cout << "column_eq_ne:\n";
  column_eq_ne();

  cout << "column_lt_le:\n";
  column_lt_le();

  cout << "column_gt_ge:\n";
  column_gt_ge();

  cout << "column_in:\n";
  column_in();

  cout << "column_starts_ends_with:\n";
  column_starts_ends_with();

  cout << "column_substr:\n";
  column_substr();

  cout << "column_concat:\n";
  column_concat();

  cout << "column_to_string:\n";
  column_to_string();

  cout << "mtuple_get:\n";
  mtuple_get();

  cout << "mtuple_ctrs:\n";
  mtuple_ctrs();

  cout << "mtuple_size:\n";
  mtuple_size();

  cout << "mtuple_three_way:\n";
  mtuple_three_way();

  cout << "row_col:\n";
  row_col();

  cout << "row_ctrs:\n";
  row_ctrs();

  cout << "row_ncols:\n";
  row_ncols();

  cout << "row_three_way:\n";
  row_three_way();

  cout << "row_all:\n";
  row_all();

  cout << "row_any:\n";
  row_any();

  cout << "table_add:\n";
  table_add();

  cout << "table_addu:\n";
  table_addu();

  cout << "table_append:\n";
  table_append();

  cout << "table_at:\n";
  table_at();

  cout << "table_col:\n";
  table_col();

  cout << "table_ctrs:\n";
  table_ctrs();

  cout << "table_deltas:\n";
  table_deltas();

  cout << "table_differ:\n";
  table_differ();

  cout << "table_distinct:\n";
  table_distinct();

  cout << "table_sort_by:\n";
  table_sort_by();

  cout << "table_rsort_by:\n";
  table_rsort_by();

  cout << "table_except:\n";
  table_except();

  cout << "table_eq_ne:\n";
  table_eq_ne();

  cout << "table_lt_le:\n";
  table_lt_le();

  cout << "table_gt_ge:\n";
  table_gt_ge();

  cout << "table_take:\n";
  table_take();

  cout << "table_rtake:\n";
  table_rtake();

  cout << "table_ij:\n";
  table_ij();

  cout << "table_aj:\n";
  table_aj();

  cout << "table_irow:\n";
  table_irow();

  cout << "table_lj:\n";
  table_lj();

  cout << "table_uj:\n";
  table_uj();

  cout << "table_ncols_nrows:\n";
  table_ncols_nrows();

  cout << "table_remove:\n";
  table_remove();

  cout << "table_reverse:\n";
  table_reverse();

  cout << "table_fills:\n";
  table_fills();

  cout << "table_select:\n";
  table_select();

  cout << "table_drop:\n";
  table_drop();

  cout << "table_rdrop:\n";
  table_rdrop();

  cout << "table_update_at:\n";
  table_update_at();

  cout << "table_update:\n";
  table_update();

  cout << "table_updatef:\n";
  table_updatef();

  cout << "table_min_max:\n";
  table_min_max();

  cout << "table_sum:\n";
  table_sum();

  cout << "table_avg:\n";
  table_avg();

  cout << "gtable_n_groups:\n";
  gtable_n_groups();

  cout << "gtable_m:\n";
  gtable_m();

  cout << "gtable_n_groups:\n";
  gtable_n_groups();

  cout << "gtable_m:\n";
  gtable_m();

  cout << "gtable_contains:\n";
  gtable_contains();

  cout << "gtable_get:\n";
  gtable_get();

  cout << "gtable_take_drop:\n";
  gtable_take_drop();

  cout << "gtable_rtake_rdrop:\n";
  gtable_rtake_rdrop();

  cout << "gtable_m:\n";
  gtable_m();

  cout << "gtable_n_groups:\n";
  gtable_n_groups();

  cout << "gtable_ungroup:\n";
  gtable_ungroup();

  cout << "gtable_reverse:\n";
  gtable_reverse();

  cout << "gtable_fills:\n";
  gtable_fills();

  cout << "gtable_deltas:\n";
  gtable_deltas();

  cout << "gtable_distinct:\n";
  gtable_distinct();

  cout << "gtable_sort:\n";
  gtable_sort();

  cout << "gtable_sort_by:\n";
  gtable_sort_by();

  cout << "gtable_rsort_by:\n";
  gtable_rsort_by();

  cout << "gtable_min_max:\n";
  gtable_min_max();

  cout << "gtable_sum:\n";
  gtable_sum();

  cout << "gtable_avg:\n";
  gtable_avg();

  cout << "fby_agg:\n";
  fby_agg();

  cout << "fby_fupdate:\n";
  fby_fupdate();
};


