#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,double>
>;

void table_lt_le()
{
  const table_t t0(
    {"abc","def","ghi"},
    {10.0, 20.0, 30.0}
  );
  
  const table_t t1(
    {"abc","def","zoo"},
    {10.0, 20.0, 100.0}
  );
 
  const auto t_lt = t0.lt(t1); // same as calling t0 < t1
  cout << t_lt << "\n";

  const auto t_le = t0.le(t1); // same as calling t0 <= t1
  cout << t_le << "\n";
}
