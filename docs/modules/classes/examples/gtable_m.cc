#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>,
  pair<c2,double>
>;


void gtable_m()
{
  const table_t t(
      {"abc", "def", "ghi", "abc", "def", "ghi"},
      {100,200,300,400,500,600},
      {1.0,2.0,3.0,4.0,5.0,6.0}
    );

  using gtable_t = table_t::group_by_t<tuple<c0>>;
  const gtable_t gt = t.group_by<c0>();
  for ( const auto& p : gt.m() )
  {
    cout << "row:\n" << p.first << "\n"; 
    cout << "table:\n" << p.second << "\n"; 
    cout << "\n";
  }
}
