#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,char>,
  pair<c2,unsigned>
>;

void table_rsort_by()
{
  const table_t t(
      {"foo","abc","zoo","abc"},
      {'a','b','c','d'},
      {1,2,3,4}
    );
  
  cout << t.rsort_by<c0,c1>() << "\n";
}
