#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_is_none()
{
  constexpr unsigned uint_none = prim_traits<unsigned>::none;
  const column<unsigned> xs ({ 0, uint_none, uint_none, 3, 4});

  const column<bool> bs = xs.is_none(); // true indicates element is none
  cout << xs << "\n";
  cout << xs.is_none() << "\n";
  cout << bs.neg() << "\n";
  const vixs_t ixs = bs.neg().where();
  for ( const auto ix : ixs ) { cout << "ix: " << ix << "\n"; }

  // show only non-none elements
  const column<unsigned> ys = xs.at( xs.is_none().neg().where() );
  cout << ys << "\n";
}
