#include <cpptables/table.hh>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>,
  pair<c2,unsigned>
>;



void gtable_rsort_by()
{
  const table_t t(
      {"abc", "def", "ghi", "abc","def","ghi","abc","def","ghi"},
      {10,20,30,1,2,3,11,12,13},
      {100,200,300,1,2,3,10,20,30}
    );

  using gtable_t = table_t::group_by_t<tuple<c0>>;

  const gtable_t gt = t.group_by<c0>().rsort_by<c1>();
  cout << gt << "\n";
}
