#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>,
  pair<c2,double>
>;


void gtable_take_drop()
{
  const table_t t(
      {"abc", "def", "ghi", "abc", "def", "ghi", "abc", "def","ghi"},
      {100,200,300,400,500,600,700,800,900},
      {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0}
    );

  using gtable_t = table_t::group_by_t<tuple<c0>>;

  const gtable_t gt = t.group_by<c0>();

  cout << gt.take(2) << "\n";
  cout << gt.drop(2) << "\n";
}
