#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_starts_ends_with()
{
  using sv_column = column<std::string_view>;
  const sv_column xs({ 
    "foo bar",
    "foo abc def",
    "hello world",
    "foo abc def"
  });


  cout << xs.starts_with("foo") << "\n";
  cout << xs.starts_with( sv_column({"foo","bar", "foo", "hello"}) ) << "\n";

  cout << xs.ends_with("bar") << "\n";
  cout << xs.ends_with( sv_column({"bar","bar", "world", "foo"}) ) << "\n";
}
