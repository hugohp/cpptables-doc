#include <cpptables/table.hh>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>,
  pair<c2,double>
>;



void table_fills()
{
  constexpr unsigned uint_none = prim_traits<unsigned>::none;
  constexpr double dbl_none = prim_traits<double>::none;

  const table_t t(
      {"abc", "def", "ghi", "foo", "bar"},
      {100,200,uint_none, 400, uint_none},
      {dbl_none, 2.0, 3.0, dbl_none, dbl_none}
    );

  using row_t = table_t::row_t;

  cout << t.fills() << "\n";
  cout << t.fill_with( row_t( "", 999, 9.9) ) << "\n";
  cout << t.zfill() << "\n";
}
