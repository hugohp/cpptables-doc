#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>
>;

void table_add()
{
  const table_t t(
      {"abc", "def", "ghi"},
      {100,200,300}
    );

  using table1_t = 
    table_t::add_t<pair<c2,double>>; // type of table with c2 column added

  const table1_t t1 = t.add<c2>( column<double>({1.0, 2.0, 3.0}) );
  cout << t1 << "\n";
}
