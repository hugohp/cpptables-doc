#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_in()
{
  const column<unsigned> c0({ 1, 2, 3, 4 ,5});
  const column<unsigned> c1({ 1, 5});

  cout << c0.in(c1) << "\n";
}
