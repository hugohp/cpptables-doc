#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_cast_to()
{
  const column<unsigned> xs ({ 1, 2, 3, 4, 5});
  const column<double> ys = xs.cast_to<double>();
  cout << ys << "\n";
}
