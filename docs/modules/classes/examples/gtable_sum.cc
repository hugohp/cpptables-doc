#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>,
  pair<c2,double>
>;

void gtable_sum()
{
  const table_t t(
      {"abc","def","ghi","abc","def","ghi","abc","def","ghi"},
      {10,100,1000,11,200,2000,12,300,3000},
      {2.0,2.0,2.0,3.0,3.0,3.0,1.0,1.0,1.0}
    );
  
  const table_t s = t.group_by<c0>().sum();
  cout << s << "\n";
}
