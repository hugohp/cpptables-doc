#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{};
struct c1{};

using mtuple_t = mtuple<
  pair<c0,string_view>,
  pair<c1,double>
>;

void mtuple_three_way()
{
  const mtuple_t m0( "abc", 100 );
  const mtuple_t m1( "abc", 200 );
  cout << (m0 < m1) << "\n";
}
