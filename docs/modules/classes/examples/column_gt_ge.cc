#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_gt_ge()
{
  const column<unsigned> c0({ 1, 2, 3, 4 ,5});
  const column<unsigned> c1({ 1, 2, 10, 20, 30});

  {
    const column<bool> r0 = c0.gt(c1); // same as c0 > c1
    cout << r0 << "\n";
    const column<bool> r1 = c0.ge(c1); // same as c0 >= c1
    cout << r1 << "\n";
  }

  {
    const column<bool> r0 = c0.gt(3); // same as c0 > 3
    cout << r0 << "\n";
    const column<bool> r1 = c0.ge(3); // same as c0 >= 3
    cout << r1 << "\n";
  }
}
