#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_min_max()
{
  const column<unsigned> xs({ 1, 2, 3, 4, 5});

  cout << xs.min() << "\n";
  cout << xs.max() << "\n";
}
