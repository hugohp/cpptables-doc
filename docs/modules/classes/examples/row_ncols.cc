#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{};
struct c1{};

using row_t = row<
  pair<c0,string_view>,
  pair<c1,double>
>;

void row_ncols()
{
  const row_t row( "abc", 100 );
  cout << row.ncols() << "\n";
}
