#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_norm()
{
  const column<unsigned> xs({ 2, 2});

  cout << xs.norm() << "\n";
}
