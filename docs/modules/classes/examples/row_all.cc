#include <cpptables/table.hh>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };

void row_all()
{
  using row_t = row<
    pair<c0,bool>,
    pair<c1,string_view>,
    pair<c2,unsigned>
  >;

  const row_t r0 = row_t(true, "foo", 10U);
  cout << r0.all() << "\n";

  const row_t r1 = row_t(true, "", 10U);
  cout << r1.all() << "\n";

  const row_t r2 = row_t(true, "bar", 0U);
  cout << r2.all() << "\n";
}
