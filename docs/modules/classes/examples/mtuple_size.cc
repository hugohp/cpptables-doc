#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{};
struct c1{};

using mtuple_t = mtuple<
  pair<c0,string_view>,
  pair<c1,double>
>;

void mtuple_size()
{
  const mtuple_t mt( "abc", 100 );
  cout << mt.size() << "\n";
}
