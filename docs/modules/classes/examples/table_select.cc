#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };

using table_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>,
  pair<c2,double>
>;


void table_select()
{
  const table_t t(
      {"abc", "def", "ghi"},
      {100,200,300},
      {1.0,2.0,3.0}
    );

  using ss_t = tuple<c0,c2>;
  using table1_t = table_t::select_t<ss_t>; // type of table with c0,c1 columns

  const table1_t t1 = t.select<c0,c2>();
  cout << t1 << "\n";

  const table1_t t2 = t.select<ss_t>();
  cout << t2 << "\n";
}
