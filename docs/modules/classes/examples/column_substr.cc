#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_substr()
{
  using sv_column = column<std::string_view>;
  using sz_column = column<std::size_t>;

  const sv_column xs({ 
    "abc def ghi",
    "foo bar xyz",
    "hello world"
  });


  cout << xs.substr(0,3) << "\n";
  cout << xs.substr( 
    sz_column({0, 4, 6}),
    sz_column({3, 3, 5})
  ) << "\n";
}
