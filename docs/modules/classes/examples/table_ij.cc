#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };

using table0_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>
>;

using table1_t = table<
  pair<c0,string_view>,
  pair<c1,unsigned>,
  pair<c2,double>
>;

void table_ij()
{
  const table0_t t0(
      {"abc", "def", "ghi", "jkl", "abc", "def", "ghi", "jkl"},
      {1,2,3,4,5,6,7,8}
    );

  constexpr unsigned uint_none = prim_traits<unsigned>::none;
  const table1_t t1(
      {"abc", "def","ghi"},
      {uint_none, 20, 30},
      {1.0, 2.0, 3.0}
    );

  const auto r0 = t0.ij( t1.key_by<c0>() );
  cout << "ij:\n" << r0 << "\n";

  const auto r1 = t0.ijf( t1.key_by<c0>() );
  cout << "ijf:\n" << r1 << "\n";
}
