#include <cpptables/table.hh>

using namespace tables;
using namespace std;

void column_all()
{
  cout << column<bool>({true,false,true}).all() << "\n";
  cout << column<string_view>({"foo","bar",""}).all() << "\n";
  cout << column<unsigned>({10,20,30}).all() << "\n";
}
