// tag::joins_intro[]
#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };
// end::joins_intro[]

void joins()
{
  // tag::joins_t0[]
  using table0_t = table<
    pair<c0,string_view>,
    pair<c1,int>
  >;

  const table0_t t0(
      {"abc", "def", "ghi", "abc", "def", "ghi"},
      {100,200,300,400,500,600}
  );
  cout << "t0:\n" << t0 << "\n";
  // end::joins_t0[]

  // tag::joins_t1[]
  using table1_t = table<
    pair<c0,string_view>,
    pair<c2,unsigned>
  >;

  const table1_t t1(
      {"abc", "def", "foo"},
      {1,2,3}
  );
  cout << "t1:\n" << t1 << "\n";
  // end::joins_t1[]

  // tag::joins_lj[]
  const auto t_lj = t0.lj( t1.key_by<c0>() );
  cout << "t_lj:\n" << t_lj << "\n";
  // end::joins_lj[]
 
  // tag::joins_ij[]
  const auto t_ij = t0.ij( t1.key_by<c0>() );
  cout << "t_ij:\n" << t_lj << "\n";
  // end::joins_ij[]

  // tag::joins_uj[]
  const auto t_uj = t0.uj( t1 );
  cout << "t_uj:\n" << t_lj << "\n";
  // end::joins_uj[]
}
