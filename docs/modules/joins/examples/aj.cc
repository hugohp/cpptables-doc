// tag::aj_intro[]
#include <cpptables/table.hh>
#include <iostream>

using namespace tables;
using namespace std;

struct c0{ constexpr static string_view name =  "Col0"; };
struct c1{ constexpr static string_view name =  "Col1"; };
struct c2{ constexpr static string_view name =  "Col2"; };
// end::aj_intro[]

void aj()
{
  // tag::aj_t0[]
  using table0_t = table<
    pair<c0,string_view>,
    pair<c1,unsigned>
  >;

  const table0_t t0(
      {"abc","def","ghi"},
      {0,5,10}
  );
  cout << "t0:\n" << t0 << "\n";
  // end::aj_t0[]

  // tag::aj_t1[]
  using table1_t = table<
    pair<c0,string_view>,
    pair<c1,unsigned>,
    pair<c2,unsigned>
  >;

  const table1_t t1(
    {"abc","def","ghi","abc","def","ghi","abc","def","ghi"},
    {1,2,3,4,5,6,7,8,9},
    {100,200,300,400,500,600,700,800,900}
  );
  cout << "t1:\n" << t1 << "\n";
  // end::aj_t1[]

  // tag::aj_tres[]
  const table1_t tres = t0.aj<c1>( t1.key_by<c0>() );
  cout << "tres:\n" << tres << "\n";
  // end::aj_tres[]
}
