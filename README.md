# cpptables-doc

Documentation for cpptables. You can view the published version online [here](https://hugohp.codeberg.page/@cpptables/public/cpptables/latest/).

## Building example code

Documentation contain code examples. To build:

```
mkdir build && cd build
cmake ..
make
```

## Generate html

```
./generate.sh
```
