cmake_minimum_required (VERSION 3.5.1)
project (cpptables-doc)

find_package(tuple_ext REQUIRED)
find_package(cpptables REQUIRED)

add_compile_options(-Wall -std=c++20)

add_subdirectory (docs/modules/ROOT/examples)
add_subdirectory (docs/modules/nones/examples)
add_subdirectory (docs/modules/joins/examples)
add_subdirectory (docs/modules/grouping/examples)
add_subdirectory (docs/modules/classes/examples)
